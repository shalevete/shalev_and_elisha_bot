import math

def do_turn(pw):
    if len(pw.my_fleets()) >= 1:
        return

    if len(pw.my_planets()) == 0:
        return

    mydict = {}
    length = 0

    for myP in pw.my_planets():
        for p in pw.neutral_planets():
            length = int(math.sqrt( (p.x() - myP.x())**2 + (p.y() - myP.y())**2 ))
            mydict[length] = [myP,p]

    for myP in pw.my_planets():
        for p in pw.enemy_planets():
            length = int(math.sqrt( (p.x() - myP.x())**2 + (p.y() - myP.y())**2 ))
            mydict[length] = [myP,p]

    myDict = sorted(mydict)

    for key, value in mydict.items():
        if value[0].num_ships() >  value[1].num_ships()*2:
                pw.issue_order(value[0], value[1], value[1].num_ships()*1.5)
                break
